package com.gadgetmart.backend.dao;

import com.gadgetmart.backend.model.AdminUser;

public interface AdminUserDao {
    public AdminUser findByUsernameAndPassword(String username,String password) throws Exception;
}
