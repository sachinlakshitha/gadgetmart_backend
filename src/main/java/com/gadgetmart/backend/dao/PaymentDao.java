package com.gadgetmart.backend.dao;

import com.gadgetmart.backend.model.Payment;

public interface PaymentDao {
    public int save(Payment payment) throws Exception;
    public Payment findByOrderId(String orderId) throws Exception;
}
