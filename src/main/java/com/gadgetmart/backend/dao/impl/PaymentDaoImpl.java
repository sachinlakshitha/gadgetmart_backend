package com.gadgetmart.backend.dao.impl;

import com.gadgetmart.backend.dao.PaymentDao;
import com.gadgetmart.backend.model.Customer;
import com.gadgetmart.backend.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PaymentDaoImpl implements PaymentDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(Payment payment) throws Exception {
        String sql = "INSERT INTO PAYMENT (PAYMENT_ID,ORDER_ID,PAYMENT_METHOD,AMOUNT,CARD_OWNER," +
                "CARD_NUMBER,EXPIRY_DATE,CVV_CODE) VALUES (:id,:orderid,:paymentmethod,:amount,:owner," +
                ":number,:expirydate,:cvvcode)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", payment.getId());
        mapSqlParameterSource.addValue("orderid", payment.getOrderId());
        mapSqlParameterSource.addValue("paymentmethod", payment.getPaymentMethod());
        mapSqlParameterSource.addValue("amount", payment.getAmount());
        mapSqlParameterSource.addValue("owner", payment.getCardOwner());
        mapSqlParameterSource.addValue("number", payment.getCardNumber());
        mapSqlParameterSource.addValue("expirydate", payment.getExpiryDate());
        mapSqlParameterSource.addValue("cvvcode", payment.getCvvCode());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public Payment findByOrderId(String orderId) throws Exception {
        String sql = "SELECT * FROM PAYMENT WHERE ORDER_ID=?";

        List<Payment> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Payment>>() {

                @Override
                public List<Payment> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Payment> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Payment payment = getPayment(resultSet);
                            listTemp.add(payment);
                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            }, orderId);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return !list.isEmpty() ? list.get(0) : null;
    }

    private Payment getPayment(ResultSet resultSet) throws SQLException,Exception {
        Payment payment = new Payment();

        payment.setId(resultSet.getString("PAYMENT_ID"));
        payment.setOrderId(resultSet.getString("ORDER_ID"));
        payment.setPaymentMethod(resultSet.getString("PAYMENT_METHOD"));
        payment.setAmount(resultSet.getDouble("AMOUNT"));
        payment.setCardOwner(resultSet.getString("CARD_OWNER"));
        payment.setCardNumber(resultSet.getString("CARD_NUMBER"));
        payment.setExpiryDate(resultSet.getString("EXPIRY_DATE"));
        payment.setCvvCode(resultSet.getString("CVV_CODE"));

        return payment;
    }
}
