package com.gadgetmart.backend.dao.impl;

import com.gadgetmart.backend.dao.CustomerDao;
import com.gadgetmart.backend.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDaoImpl implements CustomerDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(Customer customer) throws Exception {
        String sql = "INSERT INTO CUSTOMER (CUSTOMER_ID,NAME,ADDRESS,CONTACT,EMAIL,PASSWORD) VALUES (:id,:name,:address,:contact,:email,:password)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", customer.getId());
        mapSqlParameterSource.addValue("name", customer.getName());
        mapSqlParameterSource.addValue("address", customer.getAddress());
        mapSqlParameterSource.addValue("contact", customer.getContact());
        mapSqlParameterSource.addValue("email", customer.getEmail());
        mapSqlParameterSource.addValue("password", customer.getPassword());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);

    }

    @Override
    public Customer findByEmail(String email) {
        String sql = "SELECT * FROM CUSTOMER WHERE EMAIL=?";

        List<Customer> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Customer>>() {

                @Override
                public List<Customer> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Customer> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Customer customer = getCustomer(resultSet);
                            listTemp.add(customer);
                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            }, email);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public int update(Customer customer) throws Exception {
        String sql = "UPDATE CUSTOMER SET NAME=:name,ADDRESS=:address,CONTACT=:contact,EMAIL=:email WHERE CUSTOMER_ID=:id";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", customer.getId());
        mapSqlParameterSource.addValue("name", customer.getName());
        mapSqlParameterSource.addValue("address", customer.getAddress());
        mapSqlParameterSource.addValue("contact", customer.getContact());
        mapSqlParameterSource.addValue("email", customer.getEmail());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    private Customer getCustomer(ResultSet resultSet) throws SQLException,Exception {
        Customer customer = new Customer();

        customer.setId(resultSet.getString("CUSTOMER_ID"));
        customer.setName(resultSet.getString("NAME"));
        customer.setAddress(resultSet.getString("ADDRESS"));
        customer.setContact(resultSet.getString("CONTACT"));
        customer.setEmail(resultSet.getString("EMAIL"));
        customer.setPassword(resultSet.getString("PASSWORD"));

        return customer;
    }
}
