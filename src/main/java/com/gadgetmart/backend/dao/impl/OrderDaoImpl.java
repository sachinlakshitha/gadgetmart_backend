package com.gadgetmart.backend.dao.impl;

import com.gadgetmart.backend.dao.OrderDao;
import com.gadgetmart.backend.model.AdminUser;
import com.gadgetmart.backend.model.Orders;
import com.gadgetmart.backend.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderDaoImpl implements OrderDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(Orders orders) throws Exception {
        String sql = "INSERT INTO ORDERS (ORDER_ID,CUSTOMER_ID,BILLING_FIRSTNAME,BILLING_LASTNAME,BILLING_ADDRESS," +
                "BILLING_CITY,BILLING_POSTAL_CODE,BILLING_PHONE_NUMBER,BILLING_EMAIL,DELIVERY_FIRSTNAME,DELIVERY_LASTNAME," +
                "DELIVERY_ADDRESS,DELIVERY_CITY,DELIVERY_POSTAL_CODE,DELIVERY_PHONE_NUMBER,DELIVERY_EMAIL,NOTES,AMOUNT) VALUES " +
                "(:id,:customerid,:billingfirstname,:billinglastname,:billingaddress,:billingcity,:billingpostalcode," +
                ":billingphonenumber,:billingemail,:deliveryfirstname,:deliverylastname,:deliveryaddress,:deliverycity," +
                ":deliverypostalcode,:deliveryphonenumber,:deliveryemail,:notes,:amount)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", orders.getId());
        mapSqlParameterSource.addValue("customerid", orders.getCustomerId());
        mapSqlParameterSource.addValue("billingfirstname", orders.getBillingFirstName());
        mapSqlParameterSource.addValue("billinglastname", orders.getBillingLastName());
        mapSqlParameterSource.addValue("billingaddress", orders.getBillingAddress());
        mapSqlParameterSource.addValue("billingcity", orders.getBillingCity());
        mapSqlParameterSource.addValue("billingpostalcode", orders.getBillingPostalCode());
        mapSqlParameterSource.addValue("billingphonenumber", orders.getBillingPhoneNumber());
        mapSqlParameterSource.addValue("billingemail", orders.getBillingEmail());
        mapSqlParameterSource.addValue("deliveryfirstname", orders.getDeliveryFirstName());
        mapSqlParameterSource.addValue("deliverylastname", orders.getDeliveryLastName());
        mapSqlParameterSource.addValue("deliveryaddress", orders.getDeliveryAddress());
        mapSqlParameterSource.addValue("deliverycity", orders.getDeliveryCity());
        mapSqlParameterSource.addValue("deliverypostalcode", orders.getDeliveryPostalCode());
        mapSqlParameterSource.addValue("deliveryphonenumber", orders.getDeliveryPhoneNumber());
        mapSqlParameterSource.addValue("deliveryemail", orders.getDeliveryEmail());
        mapSqlParameterSource.addValue("notes", orders.getNotes());
        mapSqlParameterSource.addValue("amount", orders.getAmount());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public List<Orders> readAllByCustomerId(String customerId) throws Exception {
        String sql = "SELECT * FROM ORDERS WHERE CUSTOMER_ID=?";

        List<Orders> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Orders>>() {

                @Override
                public List<Orders> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Orders> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Orders order = getOrder(resultSet);
                            listTemp.add(order);
                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            },customerId);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return list;
    }

    private Orders getOrder(ResultSet resultSet) throws SQLException, Exception {
        Orders order = new Orders();

        order.setId(resultSet.getString("ORDER_ID"));
        order.setCustomerId(resultSet.getString("CUSTOMER_ID"));
        order.setOrderDate(resultSet.getDate("ORDER_DATE"));
        order.setBillingFirstName(resultSet.getString("BILLING_FIRSTNAME"));
        order.setBillingLastName(resultSet.getString("BILLING_LASTNAME"));
        order.setBillingAddress(resultSet.getString("BILLING_ADDRESS"));
        order.setBillingCity(resultSet.getString("BILLING_CITY"));
        order.setBillingPostalCode(resultSet.getString("BILLING_POSTAL_CODE"));
        order.setBillingPhoneNumber(resultSet.getString("BILLING_PHONE_NUMBER"));
        order.setBillingEmail(resultSet.getString("BILLING_EMAIL"));
        order.setDeliveryFirstName(resultSet.getString("DELIVERY_FIRSTNAME"));
        order.setDeliveryLastName(resultSet.getString("DELIVERY_LASTNAME"));
        order.setDeliveryAddress(resultSet.getString("DELIVERY_ADDRESS"));
        order.setDeliveryCity(resultSet.getString("DELIVERY_CITY"));
        order.setDeliveryPostalCode(resultSet.getString("DELIVERY_POSTAL_CODE"));
        order.setDeliveryPhoneNumber(resultSet.getString("DELIVERY_PHONE_NUMBER"));
        order.setDeliveryEmail(resultSet.getString("DELIVERY_EMAIL"));
        order.setNotes(resultSet.getString("NOTES"));
        order.setAmount(resultSet.getDouble("AMOUNT"));

        return order;
    }
}
