package com.gadgetmart.backend.dao.impl;

import com.gadgetmart.backend.dao.OrderProductDao;
import com.gadgetmart.backend.model.OrderProduct;
import com.gadgetmart.backend.model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderProductDaoImpl implements OrderProductDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(OrderProduct orderProduct) throws Exception {
        String sql = "INSERT INTO ORDER_PRODUCT (ORDER_PRODUCT_ID,ORDER_ID,PRODUCT_ID,SUPPLIER_ID,QTY," +
                "UNIT_PRICE,DISCOUNT) VALUES (:id,:orderid,:productid,:supplierid,:qty,:unitprice,:discount)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", orderProduct.getId());
        mapSqlParameterSource.addValue("orderid", orderProduct.getOrderId());
        mapSqlParameterSource.addValue("productid", orderProduct.getProductId());
        mapSqlParameterSource.addValue("supplierid", orderProduct.getSupplierId());
        mapSqlParameterSource.addValue("qty", orderProduct.getQty());
        mapSqlParameterSource.addValue("unitprice", orderProduct.getUnitPrice());
        mapSqlParameterSource.addValue("discount", orderProduct.getDiscount());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public List<OrderProduct> readAllByOrderId(String orderId) throws Exception {
        String sql = "SELECT * FROM ORDER_PRODUCT WHERE ORDER_ID=?";

        List<OrderProduct> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<OrderProduct>>() {

                @Override
                public List<OrderProduct> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<OrderProduct> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            OrderProduct orderProduct = getOrderProduct(resultSet);
                            listTemp.add(orderProduct);
                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            },orderId);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return list;
    }

    private OrderProduct getOrderProduct(ResultSet resultSet) throws SQLException, Exception {
        OrderProduct orderProduct = new OrderProduct();

        orderProduct.setId(resultSet.getString("ORDER_PRODUCT_ID"));
        orderProduct.setOrderId(resultSet.getString("ORDER_ID"));
        orderProduct.setProductId(resultSet.getString("PRODUCT_ID"));
        orderProduct.setSupplierId(resultSet.getString("SUPPLIER_ID"));
        orderProduct.setUnitPrice(resultSet.getDouble("UNIT_PRICE"));
        orderProduct.setDiscount(resultSet.getDouble("DISCOUNT"));

        return orderProduct;
    }
}
