package com.gadgetmart.backend.dao.impl;

import com.gadgetmart.backend.dao.SupplierDao;
import com.gadgetmart.backend.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SupplierDaoImpl implements SupplierDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(Supplier supplier) throws Exception {
        String sql = "INSERT INTO SUPPLIER (SUPPLIER_ID,NAME,CONTACT,EMAIL,URL,USERNAME,PASSWORD) VALUES (:id,:name,:contact,:email,:url,:username,:password)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", supplier.getId());
        mapSqlParameterSource.addValue("name", supplier.getName());
        mapSqlParameterSource.addValue("contact", supplier.getContact());
        mapSqlParameterSource.addValue("email", supplier.getEmail());
        mapSqlParameterSource.addValue("url", supplier.getUrl());
        mapSqlParameterSource.addValue("username", supplier.getUsername());
        mapSqlParameterSource.addValue("password", supplier.getPassword());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public List<Supplier> readAll() throws Exception {
        String sql = "SELECT * FROM SUPPLIER WHERE IS_ENABLED=1";

        List<Supplier> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Supplier>>() {

                @Override
                public List<Supplier> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Supplier> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Supplier supplier = getSupplier(resultSet);
                            listTemp.add(supplier);
                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            });

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return list;
    }

    @Override
    public Supplier findById(String id) throws Exception {
        String sql = "SELECT * FROM SUPPLIER WHERE SUPPLIER_ID=? AND IS_ENABLED=1";

        List<Supplier> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Supplier>>() {

                @Override
                public List<Supplier> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Supplier> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Supplier supplier = getSupplier(resultSet);
                            listTemp.add(supplier);
                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            },id);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public int update(Supplier supplier) throws Exception {
        String sql = "UPDATE SUPPLIER SET NAME=:name,CONTACT=:contact,EMAIL=:email,URL=:url,USERNAME=:username,PASSWORD=:password WHERE SUPPLIER_ID=:id";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", supplier.getId());
        mapSqlParameterSource.addValue("name", supplier.getName());
        mapSqlParameterSource.addValue("contact", supplier.getContact());
        mapSqlParameterSource.addValue("email", supplier.getEmail());
        mapSqlParameterSource.addValue("url", supplier.getUrl());
        mapSqlParameterSource.addValue("username", supplier.getUsername());
        mapSqlParameterSource.addValue("password", supplier.getPassword());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public int deleteById(String id) throws Exception {
        String sql = "DELETE FROM SUPPLIER WHERE SUPPLIER_ID=?";

        Object[] args = new Object[]{id};

        return jdbcTemplate.update(sql, args);
    }

    private Supplier getSupplier(ResultSet resultSet) throws SQLException,Exception {
        Supplier supplier = new Supplier();

        supplier.setId(resultSet.getString("SUPPLIER_ID"));
        supplier.setName(resultSet.getString("NAME"));
        supplier.setContact(resultSet.getString("CONTACT"));
        supplier.setEmail(resultSet.getString("EMAIL"));
        supplier.setUrl(resultSet.getString("URL"));
        supplier.setUsername(resultSet.getString("USERNAME"));
        supplier.setPassword(resultSet.getString("PASSWORD"));
        supplier.setIsEnabled(resultSet.getInt("IS_ENABLED"));

        return supplier;
    }
}
