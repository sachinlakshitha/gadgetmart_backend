package com.gadgetmart.backend.dao.impl;

import com.gadgetmart.backend.dao.AdminUserDao;
import com.gadgetmart.backend.model.AdminUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AdminUserDaoImpl implements AdminUserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public AdminUser findByUsernameAndPassword(String username, String password) throws Exception {
        String sql = "SELECT * FROM ADMIN_USER WHERE USERNAME=? AND PASSWORD=md5(?) AND IS_ENABLED=1";

        List<AdminUser> userList = null;

        try {
            userList = jdbcTemplate.query(sql, new ResultSetExtractor<List<AdminUser>>() {

                @Override
                public List<AdminUser> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<AdminUser> userTemp = new ArrayList<AdminUser>();

                    while (resultSet.next()) {
                        try {
                            AdminUser user = getAdminUser(resultSet);
                            userTemp.add(user);
                        } catch (Exception ex) {

                        }
                    }

                    return userTemp;
                }
            }, username, password);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return !userList.isEmpty() ? userList.get(0) : null;
    }

    private AdminUser getAdminUser(ResultSet resultSet) throws SQLException, Exception {
        AdminUser user = new AdminUser();

        user.setId(resultSet.getString("ADMIN_USER_ID"));
        user.setUsername(resultSet.getString("USERNAME"));
        user.setPassword(resultSet.getString("PASSWORD"));
        user.setIsEnabled(resultSet.getInt("IS_ENABLED"));

        return user;
    }
}
