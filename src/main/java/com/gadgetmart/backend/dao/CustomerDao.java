package com.gadgetmart.backend.dao;

import com.gadgetmart.backend.model.Customer;

public interface CustomerDao {
    public int save(Customer customer) throws Exception;
    public Customer findByEmail(String email);
    public int update(Customer customer) throws Exception;
}
