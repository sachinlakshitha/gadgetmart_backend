package com.gadgetmart.backend.dao;

import com.gadgetmart.backend.model.Supplier;

import java.util.List;

public interface SupplierDao {
    public int save(Supplier supplier) throws Exception;
    public List<Supplier> readAll() throws Exception;
    public Supplier findById(String id) throws Exception;
    public int update(Supplier supplier) throws Exception;
    public int deleteById(String id) throws Exception;
}
