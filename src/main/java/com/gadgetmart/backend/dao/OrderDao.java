package com.gadgetmart.backend.dao;

import com.gadgetmart.backend.model.Orders;

import java.util.List;

public interface OrderDao {
    public int save(Orders orders) throws Exception;
    public List<Orders> readAllByCustomerId(String customerId) throws Exception;
}
