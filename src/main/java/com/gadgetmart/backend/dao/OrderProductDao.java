package com.gadgetmart.backend.dao;

import com.gadgetmart.backend.model.OrderProduct;

import java.util.List;

public interface OrderProductDao {
    public int save(OrderProduct orderProduct) throws Exception;
    public List<OrderProduct> readAllByOrderId(String orderId) throws Exception;
}
