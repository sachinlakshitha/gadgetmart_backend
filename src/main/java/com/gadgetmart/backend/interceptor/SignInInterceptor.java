/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gadgetmart.backend.interceptor;

import com.gadgetmart.backend.dto.AdminUserDto;
import com.gadgetmart.backend.util.AppConstant;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class SignInInterceptor extends HandlerInterceptorAdapter{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        HttpSession httpSession = request.getSession();
                
        if (request.getRequestURI().contains("/api")) {
            return true;
        }
        
        if (request.getRequestURI().contains("/error")) {
            return true;
        }
                
        if(AppConstant.LOGIN_URL.equals(request.getRequestURI())||AppConstant.ROOT_URL.equals(request.getRequestURI())){
            AdminUserDto user = (AdminUserDto) httpSession.getAttribute("adminuser");
            if(null!=user){
                if (response.isCommitted()){  
                    response.sendRedirect("/dashboard");
                }
            }
        }

        if(!AppConstant.LOGIN_URL.equals(request.getRequestURI())&&!AppConstant.LOGOUT_URL.equals(request.getRequestURI())){
            AdminUserDto user = (AdminUserDto) httpSession.getAttribute("adminuser");
            if(null==user){
                request.logout();
                response.sendRedirect("/logout");
            }
        }
        
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, ModelAndView mav) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, Exception excptn) throws Exception {

    }
}
