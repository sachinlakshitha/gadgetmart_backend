package com.gadgetmart.backend.controller.mvc;

import com.gadgetmart.backend.dto.SupplierDto;
import com.gadgetmart.backend.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class SupplierController {
    @Value("${application.name}")
    String APPLICATION_NAME;
    @Autowired
    private SupplierService supplierService;

    @GetMapping("/add_supplier")
    public ModelAndView viewAddSupplier() {
        ModelAndView mav = new ModelAndView("supplier/add_supplier");
        mav.addObject("title",APPLICATION_NAME + " | Supplier | Add Supplier");
        return mav;
    }

    @PostMapping(value = "/add_supplier")
    @ResponseBody
    public String saveSupplier(@RequestBody SupplierDto supplierDto) throws Exception {
        boolean isSaved = false;

        try{
            isSaved = supplierService.save(supplierDto);
        }catch (Exception e){
            return "409";
        }

        if(isSaved){
            return "200";
        }else{
            return "500";
        }
    }

    @GetMapping("/view_supplier")
    public ModelAndView viewViewSupplier() {
        ModelAndView mav = new ModelAndView("supplier/view_supplier");
        mav.addObject("title",APPLICATION_NAME + " | Supplier | View Supplier");
        return mav;
    }

    @RequestMapping("/view_supplier_dt")
    @ResponseBody
    public Map viewSupplierDt() throws Exception {

        List<SupplierDto> supplierDtoList = supplierService.readAll();

        List entityList = new ArrayList<>();

        for (SupplierDto supplierDto : supplierDtoList) {
            List entity = new ArrayList<>();

            entity.add(supplierDto.getName());
            entity.add(supplierDto.getContact());
            entity.add(supplierDto.getEmail());
            entity.add("<a href=\"../update_supplier/" + supplierDto.getId() + "\" title=\"Update\"><i class=\"fas fa-edit\"></i></a>");
            entity.add("<a href=\"javascript:void(0)\" data-id=\"" + supplierDto.getId() + "\" title=\"Delete\"><i class=\"fas fa-trash\" style=\"font-size:16px;color:red;\"></i></a>");

            entityList.add(entity);
        }

        Map responseMap = new HashMap<>();
        responseMap.put("data", entityList);
        return responseMap;
    }

    @RequestMapping(value = "/update_supplier/{id}")
    public ModelAndView viewUpdateSupplier(@PathVariable String id) throws Exception {
        SupplierDto supplierDto = supplierService.findById(id);

        ModelAndView mav = new ModelAndView("supplier/update_supplier");
        mav.addObject("title", APPLICATION_NAME + " | Update Supplier | " + supplierDto.getName());
        mav.addObject("supplier", supplierDto);

        return mav;
    }

    @PostMapping(value = "/update_supplier")
    @ResponseBody
    public String updateSupplier(@RequestBody SupplierDto supplierDto) throws Exception {
        boolean isUpdated = false;

        try{
            isUpdated = supplierService.update(supplierDto);
        }catch (Exception e){
            return "409";
        }

        if(isUpdated){
            return "200";
        }else{
            return "500";
        }
    }

    @PostMapping("/delete_supplier")
    @ResponseBody
    public String deleteSupplier(@RequestParam String id) throws Exception {
        boolean isUpdated = supplierService.deleteById(id);

        if(isUpdated){
            return "200";
        }

        return "500";
    }
}
