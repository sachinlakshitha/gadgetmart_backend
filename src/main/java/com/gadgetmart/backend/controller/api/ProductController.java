package com.gadgetmart.backend.controller.api;

import com.gadgetmart.backend.dto.CategoryProductDto;
import com.gadgetmart.backend.dto.ProductDto;
import com.gadgetmart.backend.dto.SupplierDto;
import com.gadgetmart.backend.model.Supplier;
import com.gadgetmart.backend.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private SupplierService supplierService;

    @GetMapping("/product_detail/{id}/{supplier}")
    public ResponseEntity<ProductDto> getProductDetail(@PathVariable String id, @PathVariable String supplier) throws Exception{
        SupplierDto supplierDto = supplierService.findById(supplier);

        String url = supplierDto.getUrl();

        ResponseEntity<ProductDto> response =
                restTemplate.exchange(url + "product/"+id, HttpMethod.GET, null, new ParameterizedTypeReference<ProductDto>() {});

        ProductDto productDto = response.getBody();

        String imageName = productDto.getImage();
        productDto.setImage(url+"uploads/"+imageName);

        productDto.setSupplier(supplierDto.getId());

        return ResponseEntity.ok(productDto);
    }

    @GetMapping("/product/category")
    public ResponseEntity<List<CategoryProductDto>> getProductByCategory() throws Exception{
        List<SupplierDto> supplierDtoList = supplierService.readAll();

        List<CategoryProductDto> categoryProductDtoList = new ArrayList<>();

        for (SupplierDto supplierDto : supplierDtoList) {
            String url = supplierDto.getUrl();

            ResponseEntity<List<CategoryProductDto>> response =
                    restTemplate.exchange(url + "product/category", HttpMethod.GET, null, new ParameterizedTypeReference<List<CategoryProductDto>>() {});

            categoryProductDtoList = response.getBody();

            for (CategoryProductDto categoryProductDto : categoryProductDtoList) {
                List<ProductDto> products = categoryProductDto.getProducts();

                for (ProductDto productDto: products) {
                    String imageName = productDto.getImage();
                    productDto.setImage(url+"uploads/"+imageName);

                    productDto.setSupplier(supplierDto.getId());
                }
            }
        }

        return ResponseEntity.ok(categoryProductDtoList);
    }
}
