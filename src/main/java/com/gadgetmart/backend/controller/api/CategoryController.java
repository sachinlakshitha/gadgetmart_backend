package com.gadgetmart.backend.controller.api;

import com.gadgetmart.backend.dto.CategoryDto;
import com.gadgetmart.backend.dto.SupplierDto;
import com.gadgetmart.backend.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/category")
public class CategoryController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private SupplierService supplierService;

    @GetMapping("")
    public ResponseEntity readAll() throws Exception {
        List<SupplierDto> supplierDtoList = supplierService.readAll();

        List<CategoryDto> categoryDtoList = new ArrayList<>();

        for (SupplierDto supplierDto : supplierDtoList) {
            String url = supplierDto.getUrl();

            ResponseEntity<List<CategoryDto>> response =
                    restTemplate.exchange(url + "category", HttpMethod.GET, null, new ParameterizedTypeReference<List<CategoryDto>>() {});

            categoryDtoList.addAll(response.getBody());
        }

        Collection<CategoryDto> filterCategoryDtoList = categoryDtoList.stream()
                .<Map<String, CategoryDto>> collect(HashMap::new,(m,e)->m.put(e.getName(), e), Map::putAll)
                .values();

        return ResponseEntity.ok(filterCategoryDtoList);
    }
}
