package com.gadgetmart.backend.controller.api;

import com.gadgetmart.backend.config.JwtTokenUtil;
import com.gadgetmart.backend.dto.CustomerDto;
import com.gadgetmart.backend.service.CustomerService;
import com.gadgetmart.backend.service.impl.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/customer")
public class CustomerController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private CustomerService customerService;

    @PostMapping(value = "/signup")
    public ResponseEntity<Void> signup(@RequestBody CustomerDto customerDto) throws Exception {
        boolean isSaved = false;

        try{
            isSaved = customerService.save(customerDto);
        }catch (Exception e){
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        if(isSaved){
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }else{
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/token")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody CustomerDto user) throws Exception {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new CustomerDto(userDetails.getId(), userDetails.getName(), userDetails.getAddress(), userDetails.getContact(), userDetails.getEmail(), token));
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Void> update(@RequestBody CustomerDto customerDto) throws Exception {
        boolean isUpdated = false;

        try{
            isUpdated = customerService.update(customerDto);
        }catch (Exception e){
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        if(isUpdated){
            return new ResponseEntity<Void>(HttpStatus.OK);
        }else{
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
