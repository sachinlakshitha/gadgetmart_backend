package com.gadgetmart.backend.controller.api;

import com.gadgetmart.backend.dto.CategoryDto;
import com.gadgetmart.backend.dto.ProductDto;
import com.gadgetmart.backend.dto.SupplierDto;
import com.gadgetmart.backend.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/subcategory")
public class SubCategoryController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private SupplierService supplierService;

    @GetMapping("/{name}")
    public ResponseEntity readAll(@PathVariable String name) throws Exception {
        List<SupplierDto> supplierDtoList = supplierService.readAll();

        List<ProductDto> productDtoList = new ArrayList<>();

        for (SupplierDto supplierDto : supplierDtoList) {
            String url = supplierDto.getUrl();

            ResponseEntity<List<ProductDto>> response =
                    restTemplate.exchange(url + "subcategory/"+name, HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductDto>>() {});


            for (ProductDto productDto:response.getBody()) {
                String imageName = productDto.getImage();
                productDto.setImage(url+"uploads/"+imageName);

                productDto.setSupplier(supplierDto.getId());

                productDtoList.add(productDto);
            }

        }

        return ResponseEntity.ok(productDtoList);
    }
}
