package com.gadgetmart.backend.controller.api;

import com.gadgetmart.backend.dto.OrdersDto;
import com.gadgetmart.backend.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping(value = "")
    public ResponseEntity<Void> saveOrder(@RequestBody OrdersDto ordersDto) throws Exception {
        boolean isSaved = false;

        try {
            isSaved = orderService.save(ordersDto);
        } catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        if (isSaved) {
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<OrdersDto>> readAll(@PathVariable String id) throws Exception {
        return ResponseEntity.ok(orderService.readAllByCustomerId(id));
    }
}