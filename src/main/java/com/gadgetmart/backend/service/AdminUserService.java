package com.gadgetmart.backend.service;


import com.gadgetmart.backend.dto.AdminUserDto;

public interface AdminUserService {
    public AdminUserDto findByUsernameAndPassword(String username, String password) throws Exception;
}
