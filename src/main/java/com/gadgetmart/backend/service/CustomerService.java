package com.gadgetmart.backend.service;

import com.gadgetmart.backend.dto.CustomerDto;
import com.gadgetmart.backend.model.Customer;

public interface CustomerService {
    public boolean save(CustomerDto customerDto) throws Exception;
    public boolean update(CustomerDto customerDto) throws Exception;
}
