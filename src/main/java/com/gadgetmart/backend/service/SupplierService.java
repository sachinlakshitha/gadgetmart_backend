package com.gadgetmart.backend.service;

import com.gadgetmart.backend.dto.SupplierDto;
import com.gadgetmart.backend.model.Supplier;

import java.util.List;

public interface SupplierService {
    public boolean save(SupplierDto supplierDto) throws Exception;
    public List<SupplierDto> readAll() throws Exception;
    public SupplierDto findById(String id) throws Exception;
    public boolean update(SupplierDto supplierDto) throws Exception;
    public boolean deleteById(String id) throws Exception;
}
