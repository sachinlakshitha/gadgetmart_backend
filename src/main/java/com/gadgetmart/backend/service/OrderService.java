package com.gadgetmart.backend.service;

import com.gadgetmart.backend.dto.OrdersDto;

import java.util.List;

public interface OrderService {
    public boolean save(OrdersDto ordersDto) throws Exception;
    public List<OrdersDto> readAllByCustomerId(String customerId) throws Exception;
}
