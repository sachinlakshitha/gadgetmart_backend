package com.gadgetmart.backend.service.impl;

import com.gadgetmart.backend.dao.SupplierDao;
import com.gadgetmart.backend.dto.SupplierDto;
import com.gadgetmart.backend.model.Supplier;
import com.gadgetmart.backend.service.SupplierService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Transactional
@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;
    @Autowired
    private ModelMapper mapper;
    @Override
    public boolean save(SupplierDto supplierDto) throws Exception {
        supplierDto.setId(UUID.randomUUID().toString());

        Supplier supplier = getSupplier(supplierDto);

        int isSaved = supplierDao.save(supplier);

        if(isSaved == 1){
            return true;
        }

        return false;
    }

    @Override
    public List<SupplierDto> readAll() throws Exception {
        List<Supplier> suppliers = supplierDao.readAll();

        return suppliers.stream()
                .map(this::getSupplierDto)
                .collect(Collectors.toList());
    }

    @Override
    public SupplierDto findById(String id) throws Exception {
        return getSupplierDto(supplierDao.findById(id));
    }

    @Override
    public boolean update(SupplierDto supplierDto) throws Exception {
        Supplier supplier = getSupplier(supplierDto);

        int isUpdated = supplierDao.update(supplier);

        if(isUpdated == 1){
            return true;
        }

        return false;
    }

    @Override
    public boolean deleteById(String id) throws Exception {
        return supplierDao.deleteById(id) == 1 ? true:false;
    }

    private Supplier getSupplier(SupplierDto supplierDto) {
        Supplier supplier = mapper.map(supplierDto, Supplier.class);
        return supplier;
    }

    private SupplierDto getSupplierDto(Supplier supplier) {
        SupplierDto supplierDto = mapper.map(supplier, SupplierDto.class);
        return supplierDto;
    }
}
