package com.gadgetmart.backend.service.impl;

import com.gadgetmart.backend.dao.CustomerDao;
import com.gadgetmart.backend.dto.CustomerDto;
import com.gadgetmart.backend.dto.SupplierDto;
import com.gadgetmart.backend.model.Customer;
import com.gadgetmart.backend.model.Supplier;
import com.gadgetmart.backend.service.CustomerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Transactional
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public boolean save(CustomerDto customerDto) throws Exception {
        customerDto.setId(UUID.randomUUID().toString());
        customerDto.setPassword(bcryptEncoder.encode(customerDto.getPassword()));

        Customer customer = getCustomer(customerDto);

        return customerDao.save(customer) == 1 ? true:false;
    }

    @Override
    public boolean update(CustomerDto customerDto) throws Exception {
        return customerDao.update(getCustomer(customerDto)) == 1 ? true:false;
    }

    private Customer getCustomer(CustomerDto customerDto) {
        Customer customer = mapper.map(customerDto, Customer.class);
        return customer;
    }
}
