package com.gadgetmart.backend.service.impl;

import com.gadgetmart.backend.dao.AdminUserDao;
import com.gadgetmart.backend.dto.AdminUserDto;
import com.gadgetmart.backend.model.AdminUser;
import com.gadgetmart.backend.service.AdminUserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class AdminUserServiceImpl implements AdminUserService {
    @Autowired
    private AdminUserDao adminUserDao;
    @Autowired
    private ModelMapper mapper;

    @Override
    public AdminUserDto findByUsernameAndPassword(String username, String password) throws Exception {
        return getAdminUserDto(adminUserDao.findByUsernameAndPassword(username, password));
    }

    private AdminUserDto getAdminUserDto(AdminUser adminUser) {
        AdminUserDto adminUserDto = mapper.map(adminUser, AdminUserDto.class);
        return adminUserDto;
    }
}
