package com.gadgetmart.backend.service.impl;

import com.gadgetmart.backend.dao.OrderDao;
import com.gadgetmart.backend.dao.OrderProductDao;
import com.gadgetmart.backend.dao.PaymentDao;
import com.gadgetmart.backend.dao.SupplierDao;
import com.gadgetmart.backend.dto.*;
import com.gadgetmart.backend.model.OrderProduct;
import com.gadgetmart.backend.model.Orders;
import com.gadgetmart.backend.model.Payment;
import com.gadgetmart.backend.model.Supplier;
import com.gadgetmart.backend.service.OrderService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Transactional
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private SupplierDao supplierDao;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private PaymentDao paymentDao;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ModelMapper mapper;

    @Override
    public boolean save(OrdersDto ordersDto) throws Exception {
        ordersDto.setId(UUID.randomUUID().toString());

        Orders order = getOrder(ordersDto);

        List<OrderProductDto> orderProductDtoList = ordersDto.getProducts();

        List<OrderProduct> orderProducts = new ArrayList<>();

        Map<String,List<OrderProductDto>> productMap = new HashMap<>();

        for (OrderProductDto orderProductDto:orderProductDtoList) {
            orderProductDto.setId(UUID.randomUUID().toString());
            orderProductDto.setOrderId(order.getId());

            if(productMap.containsKey(orderProductDto.getSupplierId())){
                List<OrderProductDto> orderProductList = productMap.get(orderProductDto.getSupplierId());

                orderProductList.add(orderProductDto);
            }else{
                List<OrderProductDto> orderProductList = new ArrayList<>();

                orderProductList.add(orderProductDto);

                productMap.put(orderProductDto.getSupplierId(),orderProductList);
            }

            OrderProduct orderProduct = getOrderProduct(orderProductDto);

            orderProducts.add(orderProduct);
        }

        Payment payment = getPayment(ordersDto.getPayment());

        payment.setId(UUID.randomUUID().toString());
        payment.setOrderId(order.getId());

        int isOrderSaved = orderDao.save(order);

        if(isOrderSaved == 1){
            for (OrderProduct orderProduct:orderProducts) {
                orderProductDao.save(orderProduct);
            }

            int isPaymentSaved = paymentDao.save(payment);

            if(isPaymentSaved == 1){
                for (Map.Entry<String, List<OrderProductDto>> entry : productMap.entrySet()) {
                    String key = entry.getKey();
                    List<OrderProductDto> orderProductList = (List<OrderProductDto>)entry.getValue();

                    double subTotal = 0;

                    for (OrderProductDto orderProduct:orderProductList) {
                        double unitPrice = orderProduct.getUnitPrice();
                        double discount = orderProduct.getDiscount();
                        int qty = orderProduct.getQty();

                        subTotal = subTotal + ((unitPrice - (unitPrice / 100 * discount)) * qty);
                    }

                    Supplier supplier = supplierDao.findById(key);

                    String url = supplier.getUrl();

                    ordersDto.setAmount(subTotal);
                    ordersDto.getPayment().setAmount(subTotal);
                    ordersDto.setProducts(orderProductList);

                    restTemplate.postForEntity(url + "order", ordersDto, String.class);

                }
            }

            return true;
        }

        return false;
    }

    @Override
    public List<OrdersDto> readAllByCustomerId(String customerId) throws Exception {
        List<Orders> orders = orderDao.readAllByCustomerId(customerId);

        List<OrdersDto> orderDtoList = new ArrayList<>();

        for (Orders order:orders) {
            List<OrderProduct> orderProducts = orderProductDao.readAllByOrderId(order.getId());
            List<OrderProductDto> orderProductDtoList = new ArrayList<>();

            for (OrderProduct orderProduct:orderProducts) {
                String supplierId = orderProduct.getSupplierId();
                String productId = orderProduct.getProductId();

                Supplier supplier = supplierDao.findById(supplierId);

                String url = supplier.getUrl();

                ResponseEntity<ProductDto> response =
                        restTemplate.exchange(url + "product/"+productId, HttpMethod.GET, null, new ParameterizedTypeReference<ProductDto>() {});

                ProductDto productDto = response.getBody();

                String imageName = productDto.getImage();
                productDto.setImage(url+"uploads/"+imageName);

                productDto.setSupplier(supplier.getId());

                OrderProductDto orderProductDto = getOrderProductDto(orderProduct);
                orderProductDto.setProduct(productDto);

                orderProductDtoList.add(orderProductDto);
            }

            OrdersDto orderDto = getOrderDto(order);
            orderDto.setProducts(orderProductDtoList);
            orderDto.setPayment(getPaymentDto(paymentDao.findByOrderId(order.getId())));

            orderDtoList.add(orderDto);
        }

        return orderDtoList;
    }

    private Orders getOrder(OrdersDto ordersDto) {
        Orders orders = mapper.map(ordersDto, Orders.class);
        return orders;
    }

    private OrdersDto getOrderDto(Orders order) {
        OrdersDto ordersDto = mapper.map(order, OrdersDto.class);
        return ordersDto;
    }

    private OrderProduct getOrderProduct(OrderProductDto orderProductDto) {
        OrderProduct orderProduct = mapper.map(orderProductDto, OrderProduct.class);
        return orderProduct;
    }

    private OrderProductDto getOrderProductDto(OrderProduct orderProduct) {
        OrderProductDto orderProductDto = mapper.map(orderProduct, OrderProductDto.class);
        return orderProductDto;
    }

    private Payment getPayment(PaymentDto paymentDto) {
        Payment payment = mapper.map(paymentDto, Payment.class);
        return payment;
    }

    private PaymentDto getPaymentDto(Payment payment) {
        PaymentDto paymentDto = mapper.map(payment, PaymentDto.class);
        return paymentDto;
    }
}
