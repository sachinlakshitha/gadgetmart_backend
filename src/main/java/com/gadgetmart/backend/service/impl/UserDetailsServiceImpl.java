/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gadgetmart.backend.service.impl;

import com.gadgetmart.backend.dao.CustomerDao;
import com.gadgetmart.backend.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Sachin
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private CustomerDao customerDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = customerDao.findByEmail(username);

        if (customer == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }

       return UserDetailsImpl.build(customer);
    }
    
}
