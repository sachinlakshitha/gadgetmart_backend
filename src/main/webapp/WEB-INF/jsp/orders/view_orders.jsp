<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>View Orders</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                        <li class="breadcrumb-item active">View Orders</li>
                    </ol>
                </div>
            </div>

            <c:if test="${not empty msg}">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${msg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty warnmsg}">
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${warnmsg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty errmsg}">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <h4><i class="icon fa fa-check"></i> ${errmsg}</h4>

                </div>
            </c:if>

            <span id="msg"></span>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                                                             
                    <div class="card card-secondary">     
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table" class="table table-bordered" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Order Date</th>
                                            <th>Customer Name</th>
                                            <th>Delivery Address</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Status</th>
                                            <th>Payment Method</th>
                                            <th>Amount</th>
                                            <th style="width: 5px;"></th>
                                            <th style="width: 5px;"></th>
                                            <th style="width: 5px;"></th>
                                        </tr>   
                                    </thead>

                                    <tbody>
                                    </tbody>

                                    <tfoot>
                                    </tfoot>			
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {
        $("#view_orders").addClass("active");    
        
        var table = $('#table').DataTable({
            "pageLength": 10,
            "ajax": "/view_orders_dt"
        });
        
         $('#table tbody').on('click', 'td', function () {
            var col = $(this).parent().children().index($(this));
            var row = $(this).parent().parent().children().index($(this).parent());
                                    
            if (col === 9) {
                var id = $('#table tbody tr:eq(' + row + ')').find("td button").eq(0).data("id");
                
                updateStatus(id,"APPROVED");               
            }
            
            if(col === 10){
                var id = $('#table tbody tr:eq(' + row + ')').find("td button").eq(1).data("id");
                
                updateStatus(id,"REJECTED");         
            }

        });
    });
    
    function updateStatus(id,status){
        $.ajax({
            type: "POST",
            url: "/update_status",
            data: {
                id: id,
                status: status
            },
            success: function (result) {
               location.reload();            
            },
            error: function (result) {
                Swal.fire({
                    type: 'error',
                    title: 'Order update failed...',
                    text: ''
                });
            }
        });
    }
</script>
</body>
</html>
