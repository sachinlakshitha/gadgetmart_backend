<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>View Invoice</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                        <li class="breadcrumb-item active">View Invoice</li>
                    </ol>
                </div>
            </div>           
        </div><!-- /.container-fluid -->
    </section>
    
    <c:set var="date" value="${fn:split(orders.orderDate, ' ')}" />

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <i class="fas fa-globe"></i> ${company.name}
                                    <small class="float-right">Date: ${date[0]}</small>
                                </h4>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                From
                                <address>
                                    <strong>${company.name}</strong><br>
                                    <c:forEach var="spt" items="${fn:split(company.address, ',')}">
                                        ${spt}<br>
                                    </c:forEach>
                                    Phone: ${company.contact}<br>
                                    Email: ${company.email}
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                To
                                <address>
                                    <strong>${orders.customer.name}</strong><br>
                                    ${orders.streetAddress1}<br>
                                    Phone: ${orders.customer.contact}<br>
                                    Email: ${orders.customer.email}
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <b>Invoice #${orders.id}</b><br>
                                <br>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Qty</th>
                                            <th>Price</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${orders.orderDetails}" var="object">
                                            <tr>
                                                <td>${object.product.name} - (${object.size})</td>
                                                <td>${object.qty}</td>
                                                <td><fmt:formatNumber pattern="0.00" value="${object.product.price}" /></td>
                                                <td><fmt:formatNumber pattern="0.00" value="${object.product.price * object.qty}" /></td>

                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <!-- accepted payments column -->
                            <div class="col-6">
                                <p class="lead">Payment Methods:</p>
                                <img src="/resources/dist/img/credit/visa.png" alt="Visa">
                                <img src="/resources/dist/img/credit/mastercard.png" alt="Mastercard">
                                <img src="/resources/dist/img/credit/american-express.png" alt="American Express">
                                <img src="/resources/dist/img/credit/mestro.png" alt="Mestro">
                            </div>
                            <!-- /.col -->
                            <div class="col-6">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th style="width:50%">Subtotal:</th>
                                            <td><fmt:formatNumber pattern="0.00" value="${orders.payment.amount}" /></td>
                                        </tr>
                                        <tr>
                                            <th>Shipping:</th>
                                            <td>250.00</td>
                                        </tr>
                                        <tr>
                                            <th>Total:</th>
                                            <td><fmt:formatNumber pattern="0.00" value="${orders.payment.amount + 250.00}" /></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- this row will not appear when printing -->
                        <div class="row no-print">
                            <div class="col-12">
                                <a class="btn btn-default" onclick="window.print()"><i class="fas fa-print"></i> Print</a> 
                            </div>
                        </div>
                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {
        $("#view_orders").addClass("active");
    }); 
</script>
</body>
</html>
