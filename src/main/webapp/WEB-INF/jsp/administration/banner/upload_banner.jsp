<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Upload Banner</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
              <li class="breadcrumb-item active">Upload Banner</li>
            </ol>
          </div>
        </div>
          
        <c:if test="${not empty msg}">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                <h4><i class="icon fa fa-check"></i> ${msg}</h4>

            </div>
        </c:if>

        <c:if test="${not empty warnmsg}">
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                <h4><i class="icon fa fa-check"></i> ${warnmsg}</h4>

            </div>
        </c:if>

        <c:if test="${not empty errmsg}">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                <h4><i class="icon fa fa-check"></i> ${errmsg}</h4>

            </div>
        </c:if>

        <span id="msg"></span>
        
      </div><!-- /.container-fluid -->
    </section>
    
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">                                                             
                <div class="card card-secondary">                   
                    <form id="form" action="../upload_banner" method="post" enctype="multipart/form-data"> 
                        <div class="card-body">
                            <div class="form-group">
                                <label>DESCRIPTION:</label>
                                
                                <input type="text" class="form-control" id="description" name="description" placeholder="DESCRIPTION">
                            </div>
                            
                            <div class="form-group">
                                <label>URL:</label>
                                
                                <input type="text" class="form-control" id="url" name="url" placeholder="URL">
                            </div>
                            
                            <div class="form-group">
                                <label>BANNER:</label>
                                
                                <input type="file" class="form-control" id="banner" name="banner">
                            </div>
                        </div>
                                
                        <div class="card-footer">
                            <button type="submit" id="saveBtn" class="btn btn-primary">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {    
        $(".administration").removeClass("treeview").addClass("treeview menu-open");
        $("#administration-menu").css('display', 'block');
        $("#banner").removeClass("has-treeview").addClass("has-treeview menu-open");
        $("#banner-menu").css('display', 'block');
        $("#upload_banner").addClass("active");
        
        $('.select2').select2();
        
        $('#form').validate({
            rules: {
                banner: {
                    required: true
                },
                description: {
                    required: true
                },
                url: {
                    required: true
                }
            }
        });       
    });
</script>
</body>
</html>
