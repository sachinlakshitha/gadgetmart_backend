<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
              <li class="breadcrumb-item active">Add User</li>
            </ol>
          </div>
        </div>
          
        <c:if test="${not empty msg}">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                <h4><i class="icon fa fa-check"></i> ${msg}</h4>

            </div>
        </c:if>

        <c:if test="${not empty warnmsg}">
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                <h4><i class="icon fa fa-check"></i> ${warnmsg}</h4>

            </div>
        </c:if>

        <c:if test="${not empty errmsg}">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                <h4><i class="icon fa fa-check"></i> ${errmsg}</h4>

            </div>
        </c:if>

        <span id="msg"></span>
        
      </div><!-- /.container-fluid -->
    </section>
    
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">                                                             
                <div class="card card-secondary">                   
                    <form id="form" action="../add_user" method="post"> 
                        <div class="card-body">
                            <div class="form-group">
                                <label>USERNAME:</label>
                                
                                <input type="text" class="form-control" id="username" name="username" placeholder="USERNAME" autofocus>
                            </div>  
                            
                            <div class="form-group">
                                <label>PASSWORD:</label>
                                
                                <input type="password" class="form-control" id="password" name="password" placeholder="PASSWORD" autofocus>
                            </div> 
                            
                            <div class="form-group">
                                <label>ROLE:</label>

                                <select class="form-control select2" id="role" name="role" data-placeholder="Select a Role" style="width: 100%;">
                                    <c:forEach items="${role}" var="lst">
                                        <option value="${lst.id}">${lst.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                                
                        <div class="card-footer">
                            <button type="submit" id="saveBtn" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {    
        $(".administration").removeClass("treeview").addClass("treeview menu-open");
        $("#administration-menu").css('display', 'block');
        $("#user").removeClass("has-treeview").addClass("has-treeview menu-open");
        $("#user-menu").css('display', 'block');
        $("#add_user").addClass("active");
        
        $('.select2').select2();
        
        $('#form').validate({
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                role: {
                    required: true
                }
            }
        });       
    });
</script>
</body>
</html>
