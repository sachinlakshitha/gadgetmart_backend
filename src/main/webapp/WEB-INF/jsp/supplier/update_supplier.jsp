<%@include file="/WEB-INF/jspf/header.jspf" %>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Supplier</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Update Supplier</li>
                    </ol>
                </div>
            </div>

            <c:if test="${not empty msg}">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-check"></i> ${msg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty warnmsg}">
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-check"></i> ${warnmsg}</h4>

                </div>
            </c:if>

            <c:if test="${not empty errmsg}">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-check"></i> ${errmsg}</h4>

                </div>
            </c:if>

            <span id="msg"></span>

        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                                                             
                    <div class="card card-secondary">   
                        <form id="form">
                            <div class="card-body">
                                <input type="hidden" class="form-control" id="id" name="id" value="${supplier.id}">

                                <div class="form-group">
                                    <label>NAME:</label>

                                    <input type="text" class="form-control" id="name" name="name" placeholder="NAME" value="${supplier.name}" autofocus>
                                </div> 

                                <div class="form-group">
                                    <label>CONTACT:</label>

                                    <input type="text" class="form-control" id="contact" name="contact" value="${supplier.contact}" placeholder="CONTACT">
                                </div> 

                                <div class="form-group">
                                    <label>EMAIL:</label>

                                    <input type="email" class="form-control" id="email" name="email" value="${supplier.email}" placeholder="EMAIL">
                                </div>

                                <div class="form-group">
                                    <label>URL:</label>

                                    <input type="text" class="form-control" id="url" name="url" value="${supplier.url}" placeholder="URL">
                                </div>

                                <div class="form-group">
                                    <label>USERNAME:</label>

                                    <input type="text" class="form-control" id="username" name="username" value="${supplier.username}" placeholder="USERNAME">
                                </div>


                                <div class="form-group">
                                    <label>PASSWORD:</label>

                                    <input type="text" class="form-control" id="password" name="password" value="${supplier.password}" placeholder="PASSWORD">
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="saveBtn" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<%@include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(function () {
        $(".supplier").removeClass("treeview").addClass("treeview menu-open");
        $("#supplier-menu").css('display', 'block');
        $("#view_supplier").addClass("active");

        $('#form').validate({
            rules: {
                name: {
                    required: true
                },
                contact: {
                    required: true
                },
                email: {
                    required: true
                },
                url: {
                    required: true
                },
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            submitHandler: function (form,event) {
                event.preventDefault();

                $.ajax({
                    type: "POST",
                    url: "../update_supplier",
                    contentType: "application/json",
                    data: JSON.stringify($('#form').serializeJSON()),
                    success: function (result) {
                        if (result == "200") {
                            $('html,body').animate({scrollTop:0},800);
                            $('#msg').append('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Supplier updated successfully</h4></div>');
                        }else if (result == "409") {
                            $('html,body').animate({scrollTop:0},800);
                            $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Supplier already exists!</h4></div>');
                        } else {
                            $('html,body').animate({scrollTop:0},800);
                            $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Supplier updated failed</h4></div>');
                        }
                    },
                    error: function (err) {
                        $('html,body').animate({scrollTop:0},800);
                        $('#msg').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Supplier updated failed</h4></div>');
                    }
                });
            }
        });
    });
</script>
</body>
</html>
